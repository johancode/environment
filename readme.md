This repo uses for deploy the platform via Docker-compose utility.

# Services
1. Proxy - Nginx
2. API endpoint - PHP-FPM
3. Cache service - REDIS
4. RDBMS - MySQL
5. Search engine - ElasticSearch
6. SPA - Node/Nuxt

# Updating

0. if docker is offline - `docker-compose up -d`
1. `sh update.sh` 

# Helpers

## Remove all unused images:

```shell script
docker rm -v $(docker ps -aq -f status=exited)
```