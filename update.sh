#!/bin/bash

RED='\033[0;31m'
NC='\033[0m' # No Color

echo "╔══════════════════════╗"
echo "║ HouseRepair updating ║"
echo "╚══════════════════════╝"
echo "\n"

if [ ! -f .env ]; then
  echo ".env file not exists! (exit)"
  exit 1
fi

PROJECT_ROOT=$(pwd)

#
# ----------- API -------------
export $(cat .env | grep API_SRC_PATH)
#echo "=> api: composer install"
docker run --rm -v "$API_SRC_PATH:/app" --user $(id -u):$(id -g) composer install --ignore-platform-reqs --no-scripts
#echo "=> api: composer dumpautoload"
docker run --rm -v "$API_SRC_PATH:/app" --user $(id -u):$(id -g) composer dumpautoload
echo "=> api: artisan migrate"
docker-compose exec hr_api php artisan migrate --force

#
# ----------- SPA -------------
export $(cat .env | grep CLIENT_SRC_PATH)
echo "=> client: yarn install"
docker run --rm -v "$CLIENT_SRC_PATH:/app" -w="/app" node:13-alpine yarn install
echo "=> client: yarn build"
docker run --rm -v "$CLIENT_SRC_PATH:/app" -w="/app" node:13-alpine yarn run build

echo "docker restarting"
docker-compose restart
